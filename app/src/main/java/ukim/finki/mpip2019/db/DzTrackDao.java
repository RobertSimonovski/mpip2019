package ukim.finki.mpip2019.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import ukim.finki.mpip2019.models.DzTrack;

import java.util.List;

@Dao
public interface DzTrackDao {

    @Query("SELECT * from dz_track")
    public List<DzTrack> getAll();

    @Query("SELECT * from dz_track WHERE custom_title LIKE :title")
    public List<DzTrack> findByTitle(String title); // title == "abc"

    @Insert
    public void insert(DzTrack ...tracks);

    @Query("DELETE from dz_track WHERE id = :id")
    public void delete(Long id);

    @Query("DELETE from dz_track")
    public void deleteAll();

}
