package ukim.finki.mpip2019.models;

public class DzPlaylist {

    public Long id;

    public String title;

    public String pictureSmall;

    public DzTracksData tracks;

}
