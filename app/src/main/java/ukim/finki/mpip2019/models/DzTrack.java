package ukim.finki.mpip2019.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "dz_track")
public class DzTrack {

    @PrimaryKey
    public Long id;

    @ColumnInfo(name = "custom_title")
    public String title;

}
