package ukim.finki.mpip2019.asynctask;

import android.os.AsyncTask;
import retrofit2.Call;
import ukim.finki.mpip2019.PlaylistInterface;
import ukim.finki.mpip2019.client.DzApiClient;
import ukim.finki.mpip2019.models.DzPlaylist;

import java.io.IOException;

public class DzPlaylistAsyncTask extends AsyncTask<Long, Integer, DzPlaylist> {
    PlaylistInterface playlistInterface;

    public DzPlaylistAsyncTask(PlaylistInterface playlistInterface){
        this.playlistInterface = playlistInterface;
    }

    @Override
    protected DzPlaylist doInBackground(Long... longs) {
        final Call<DzPlaylist> playlist = DzApiClient.getService().getPlaylist(longs[0]);
        try {

            return playlist.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(DzPlaylist dzPlaylist) {
        playlistInterface.loadedDzPlaylist(dzPlaylist);
//        super.onPostExecute(dzPlaylist);
    }
}
